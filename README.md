# Initial commit of AzCATI cloud python libraries. 

This is embarassing code right now and just a functionional proof of concept.

!pip install azcati

from azcati import azcati_cloud

# Initial folders

azcati_cloud.init_folders()

# create configuration for first time and add AWS keys
azcati_cloud.init_dynamo_config()

# query all data from all ponds between start and end date
azcati_cloud.get_pond_data(['SPW15', 'SPW16', 'SPW17', 'SPW18', 'SPW19', 'SPW20'], start_date="2018-01-18", end_date="2018-02-12")
